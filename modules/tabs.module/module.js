$(document).ready(function() {

  $(window).scroll(function() {
    if ($(window).scrollTop() >= 354) {
      $(".tabbing-section").addClass("sticky");
    } else {
      $(".tabbing-section").removeClass("sticky");
    }
  });
});
