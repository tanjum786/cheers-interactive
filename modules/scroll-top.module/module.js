const topToScroll = document.querySelector(".top-to-scroll");
const scrollPositionToHide = 800;

function handleScroll() {
  const currentScrollPosition = window.scrollY;

  if (currentScrollPosition <= scrollPositionToHide) {
    topToScroll.style.display = "none";
  } else {
    topToScroll.style.display = "block";
  }
}
window.addEventListener("scroll", handleScroll);
window.addEventListener("load", function() {
  topToScroll.style.display = "none";
});
