$(document).ready(function() {
  $(".tab-section h5:first-child").addClass("active");
  $(".tab-content").hide();
  $(".tab-content:first").show();
  
  $(".tab-section h5").click(function() {
    $(".tab-section h5").removeClass("active");
    $(this).addClass("active");
    $(".tab-content").hide();
    var index = $(this).index();
    $(".tab-content")
      .eq(index)
      .show();
  });
});