document.addEventListener("DOMContentLoaded", function() {
  let blogMainContainer = document.querySelector(".blog-list");
  let Industry = blogMainContainer.getAttribute("data-original-industry");

  const displayCategories = [
    "Article",
    "Case Studies",
    "Client Events",
    "Client Video",
    "Corporate Presentations",
    "Events",
    "Webinars",
    "White Papers",
    "Infographics",
    "Reports",
    "Research Reports",
    "Product Video",
    "Press Release",
    "Podcast",
    "Round-Table Discussion",
    "Conference",
    "Fireside chat",
    "Panel Interview",
  ];

  const requestOptions = {
    method: "GET",
    headers: {
      Authorization:
        "Bearer $2b$10$P2jNMrbDJKZl4t5S274qNeR96P36LdXbT/JO2EdGrCY3xj7kOCSSK",
      "content-type": "application/json",
    },
    redirect: "follow",
  };

  let api = "https://fbrev.futurebridge.com/prod/posts";
  if (Industry !== "none") {
    api = `https://fbrev.futurebridge.com/prod/posts?industry=${Industry}&per_page=25&page=1`;
  } else {
    api = api;
  }
  let fetchPromises = [];
  let categorizedBlogs = {};
  fetch(api, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      data = result;
      data.sort((a, b) => {
        return new Date(b.published_on) - new Date(a.published_on);
      });
      for (let i = 0; i < data.length; i++) {
        let id = data[i].featured_media;
        let images;
        let defaultImage =
          "https://www.futurebridge.com/wp-includes/images/media/default.png";
        let imageFetchPromise = Promise.resolve();
        if (id > 0) {
          imageFetchPromise = fetch(
            `https://fbrev.futurebridge.com/prod/media/${id}`,
            requestOptions
          )
            .then((response) => response.json())
            .then((imageData) => {
              images =
                imageData.media_details.sizes.large?.source_url ??
                imageData.media_details.sizes.medium?.source_url ??
                imageData.media_details.sizes.medium_large?.source_url ??
                "https://23962694.fs1.hubspotusercontent-na1.net/hubfs/23962694/FutureBridge.png";
            })
            .catch((error) =>
              console.error("Error fetching image from second API", error)
            );
        }

        if (data[i].featured_image_src === defaultImage) {
          images =
            "https://23962694.fs1.hubspotusercontent-na1.net/hubfs/23962694/FutureBridge.png";
        }
        let publishDate = data[i].date;
        const dateObj = new Date(publishDate);
        const options = { year: "numeric", month: "short", day: "numeric" };
        const formattedDate = dateObj.toLocaleDateString("en-US", options);
        let encodedTitle = data[i].title.rendered;
        let parser = new DOMParser();
        let decodedDocument = parser.parseFromString(encodedTitle, "text/html");
        let decodedTitle =
          decodedDocument.body.textContent || decodedDocument.body.innerText;
        let content = data[i].content.rendered;
        content = content.replace(
          /\[\/?[^\]]*\]|<span[^>]*?\bclass="mce_SELRES_start"[^>]*>.*?<\/span>/g,
          ""
        );
        let encodeDis = content;
        let Disparser = new DOMParser();
        let decodeDoc = Disparser.parseFromString(encodeDis, "text/html");
        let decodeDis = decodeDoc.body.textContent || "";
        let link = data[i].link;

        let blogPost = {
          title: decodedTitle,
          content: decodeDis,
          link: link,
          date: formattedDate,
        };
        imageFetchPromise.then(() => {
          blogPost.images = images;
        });

        let catDecodedTitle;
        const fetchAndDisplayMatchingCategories = async () => {
          for (const categoryId of data[i].categories) {
            try {
              const response = await fetch(
                `https://fbrev.futurebridge.com/prod/categories/${categoryId}`,
                requestOptions
              );
              if (response.ok) {
                const categoryData = await response.json();
                const categoryName = categoryData.name;
                let catParser = new DOMParser();
                let catDecodedDocument = catParser.parseFromString(
                  categoryName,
                  "text/html"
                );
                catDecodedTitle =
                  catDecodedDocument.body.textContent ||
                  decodedDocument.body.innerText;

                if (displayCategories.includes(catDecodedTitle)) {
                  if (!categorizedBlogs[catDecodedTitle]) {
                    categorizedBlogs[catDecodedTitle] = [];
                  }
                  categorizedBlogs[catDecodedTitle].push(blogPost);
                  break;
                }
              } else {
                console.error(`Failed to fetch category with ID ${categoryId}`);
              }
            } catch (error) {
              console.error(
                `Error fetching category with ID ${categoryId}:`,
                error
              );
            }
          }
        };
        let fetchPromise = fetchAndDisplayMatchingCategories();
        fetchPromises.push(fetchPromise);
      }
      Promise.all(fetchPromises)
        .then(() => {
          let counters = {};
          for (let key in categorizedBlogs) {
            counters[key] = 0;
          }

          const categoryKeys = Object.keys(categorizedBlogs);
          const totalIterations =
            categoryKeys.length *
            Math.max(
              ...categoryKeys.map((key) => categorizedBlogs[key].length)
            );
          let postsDisplayed = 0;
          for (let i = 0; i < totalIterations; i++) {
            let currentCategory = categoryKeys[i % categoryKeys.length];
            if (
              counters[currentCategory] >=
              categorizedBlogs[currentCategory].length
            ) {
              continue;
            }

            let currentPost =
              categorizedBlogs[currentCategory][counters[currentCategory]];
            let ContentContainer = document.createElement("div");
            ContentContainer.setAttribute("class", "blog-content-container");

            let imageContainer = document.createElement("div");
            imageContainer.setAttribute("class", "blog-image");
            let image = document.createElement("img");
            image.src = currentPost.images;
            image.alt = "Featured Image";
            imageContainer.appendChild(image);
            ContentContainer.appendChild(imageContainer);

            let contentWrapper = document.createElement("div");
            contentWrapper.setAttribute("class", "blog-content-wrapper");

            let authorContainer = document.createElement("div");
            authorContainer.setAttribute("class", "author-content");

            let paragraph = document.createElement("p");
            let dateSpan = document.createElement("span");
            let catSpan = document.createElement("span");
            dateSpan.textContent = currentPost.date;
            catSpan.textContent = currentCategory;

            paragraph.appendChild(dateSpan);
            paragraph.appendChild(catSpan);

            authorContainer.appendChild(paragraph);
            contentWrapper.appendChild(authorContainer);

            ContentContainer.appendChild(contentWrapper);

            let title = document.createElement("h5");
            title.textContent = currentPost.title;
            let blogTitleContainer = document.createElement("div");
            blogTitleContainer.setAttribute("class", "blog-title-content");
            blogTitleContainer.appendChild(title);

            let description = document.createElement("p");
            description.setAttribute("class", "blog-dis");
            description.textContent = currentPost.content;
            blogTitleContainer.appendChild(description);

            contentWrapper.appendChild(blogTitleContainer);
            let blogButton = document.createElement("div");
            blogButton.setAttribute("class", "blog-button");
            let button = document.createElement("a");
            button.setAttribute("href", currentPost.link);
            button.setAttribute("target", "_blank");
            button.innerText = "Read More";
            let svgString = `
            <svg xmlns="http://www.w3.org/2000/svg" width="70" height="18" viewBox="0 0 70 18" fill="none">
              <g clip-path="url(#clip0_43_5570)">
                <path d="M1 8.93945H56.1427" stroke-width="1.99955" stroke-linecap="round" />
                <path d="M60.9844 16.264L68.3105 8.93951L60.9844 1.61499" stroke-width="1.99955"
                  stroke-linecap="round" stroke-linejoin="round" />
              </g>
              <defs>
                <clipPath id="clip0_43_5570">
                  <rect width="69.31" height="17.48" fill="white" transform="translate(0 0.199951)" />
                </clipPath>
              </defs>
            </svg>`;

            button.innerHTML += svgString;

            blogButton.appendChild(button);
            contentWrapper.appendChild(blogButton);

            blogMainContainer.appendChild(ContentContainer);

            if (postsDisplayed === 8) {
              break;
            }
            counters[currentCategory]++;
            postsDisplayed++;
          }

          $(".blog-list-slider").slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            autoplay: true,
            autoplaySpeed: 5000,
            slidesToScroll: 3,
            responsive: [
              {
                breakpoint: 1060,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  infinite: true,
                  dots: true,
                },
              },
              {
                breakpoint: 700,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                },
              },
            ],
          });
        })
        .catch((error) => console.error("Error fetching categories:", error));
    })
    .catch((error) => console.log("error", error));
});

// blog-list

document.addEventListener("DOMContentLoaded", function() {
  let blogMainContainer = document.querySelector(".blog_list");
  let Industry = blogMainContainer.getAttribute("data-original-industry");

  const displayCategories = [
    "Article",
    "Case Studies",
    "Client Events",
    "Client Video",
    "Corporate Presentations",
    "Events",
    "Webinars",
    "White Papers",
    "Infographics",
    "Reports",
    "Research Reports",
    "Product Video",
    "Press Release",
    "Podcast",
    "Round-Table Discussion",
    "Conference",
    "Fireside chat",
    "Panel Interview",
  ];

  const requestOptions = {
    method: "GET",
    headers: {
      Authorization:
        "Bearer $2b$10$P2jNMrbDJKZl4t5S274qNeR96P36LdXbT/JO2EdGrCY3xj7kOCSSK",
      "content-type": "application/json",
    },
    redirect: "follow",
  };
  let data = null;

  let api = "https://fbrev.futurebridge.com/prod/posts";
  if (Industry !== "none") {
    api = `https://fbrev.futurebridge.com/prod/posts?industry=${Industry}&per_page=25&page=1`;
  } else {
    api = api;
  }
  let fetchPromises = [];
  let categorizedBlogs = {};
  fetch(api, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      data = result;
      data.sort((a, b) => {
        return new Date(b.published_on) - new Date(a.published_on);
      });
      for (let i = 0; i < 3; i++) {
        let id = data[i].featured_media;
        let images;
        let defaultImage =
          "https://www.futurebridge.com/wp-includes/images/media/default.png";
        let imageFetchPromise = Promise.resolve();
        if (id > 0) {
          imageFetchPromise = fetch(
            `https://fbrev.futurebridge.com/prod/media/${id}`,
            requestOptions
          )
            .then((response) => response.json())
            .then((imageData) => {
              images =
                imageData.media_details.sizes.large?.source_url ??
                imageData.media_details.sizes.medium?.source_url ??
                imageData.media_details.sizes.medium_large?.source_url ??
                "https://23962694.fs1.hubspotusercontent-na1.net/hubfs/23962694/FutureBridge.png";
            })
            .catch((error) =>
              console.error("Error fetching image from second API", error)
            );
        }

        if (data[i].featured_image_src === defaultImage) {
          images =
            "https://23962694.fs1.hubspotusercontent-na1.net/hubfs/23962694/FutureBridge.png";
        }
        let publishDate = data[i].date;
        const dateObj = new Date(publishDate);
        const options = { year: "numeric", month: "short", day: "numeric" };
        const formattedDate = dateObj.toLocaleDateString("en-US", options);
        let encodedTitle = data[i].title.rendered;
        let parser = new DOMParser();
        let decodedDocument = parser.parseFromString(encodedTitle, "text/html");
        let decodedTitle =
          decodedDocument.body.textContent || decodedDocument.body.innerText;
        let content = data[i].content.rendered;
        content = content.replace(
          /\[\/?[^\]]*\]|<span[^>]*?\bclass="mce_SELRES_start"[^>]*>.*?<\/span>/g,
          ""
        );
        let encodeDis = content;
        let Disparser = new DOMParser();
        let decodeDoc = Disparser.parseFromString(encodeDis, "text/html");
        let decodeDis = decodeDoc.body.textContent || "";
        let link = data[i].link;

        let blogPost = {
          title: decodedTitle,
          content: decodeDis,
          link: link,
          date: formattedDate,
        };
        imageFetchPromise.then(() => {
          blogPost.images = images;
        });

        let catDecodedTitle;
        const fetchAndDisplayMatchingCategories = async () => {
          for (const categoryId of data[i].categories) {
            try {
              const response = await fetch(
                `https://fbrev.futurebridge.com/prod/categories/${categoryId}`,
                requestOptions
              );
              if (response.ok) {
                const categoryData = await response.json();
                const categoryName = categoryData.name;
                let catParser = new DOMParser();
                let catDecodedDocument = catParser.parseFromString(
                  categoryName,
                  "text/html"
                );
                catDecodedTitle =
                  catDecodedDocument.body.textContent ||
                  decodedDocument.body.innerText;

                if (displayCategories.includes(catDecodedTitle)) {
                  if (!categorizedBlogs[catDecodedTitle]) {
                    categorizedBlogs[catDecodedTitle] = [];
                  }
                  categorizedBlogs[catDecodedTitle].push(blogPost);
                  break;
                }
              } else {
                console.error(`Failed to fetch category with ID ${categoryId}`);
              }
            } catch (error) {
              console.error(
                `Error fetching category with ID ${categoryId}:`,
                error
              );
            }
          }
        };
        let fetchPromise = fetchAndDisplayMatchingCategories();
        fetchPromises.push(fetchPromise);
      }
      Promise.all(fetchPromises)
        .then(() => {
          let counters = {};
          for (let key in categorizedBlogs) {
            counters[key] = 0;
          }

          const categoryKeys = Object.keys(categorizedBlogs);
          const totalIterations =
            categoryKeys.length *
            Math.max(
              ...categoryKeys.map((key) => categorizedBlogs[key].length)
            );
          let postsDisplayed = 0;
          for (let i = 0; i < totalIterations; i++) {
            let currentCategory = categoryKeys[i % categoryKeys.length];
            if (
              counters[currentCategory] >=
              categorizedBlogs[currentCategory].length
            ) {
              continue;
            }

            let currentPost =
              categorizedBlogs[currentCategory][counters[currentCategory]];
            let ContentContainer = document.createElement("div");
            ContentContainer.setAttribute("class", "blog-content-container");

            let imageContainer = document.createElement("div");
            imageContainer.setAttribute("class", "blog-image");
            let image = document.createElement("img");
            image.src = currentPost.images;
            image.alt = "Featured Image";
            imageContainer.appendChild(image);
            ContentContainer.appendChild(imageContainer);

            let contentWrapper = document.createElement("div");
            contentWrapper.setAttribute("class", "blog-content-wrapper");

            let authorContainer = document.createElement("div");
            authorContainer.setAttribute("class", "author-content");

            let paragraph = document.createElement("p");
            let dateSpan = document.createElement("span");
            let catSpan = document.createElement("span");
            dateSpan.textContent = currentPost.date;
            catSpan.textContent = currentCategory;

            paragraph.appendChild(dateSpan);
            paragraph.appendChild(catSpan);

            authorContainer.appendChild(paragraph);
            contentWrapper.appendChild(authorContainer);

            ContentContainer.appendChild(contentWrapper);

            let title = document.createElement("h5");
            title.textContent = currentPost.title;
            let blogTitleContainer = document.createElement("div");
            blogTitleContainer.setAttribute("class", "blog-title-content");
            blogTitleContainer.appendChild(title);

            let description = document.createElement("p");
            description.setAttribute("class", "blog-dis");
            description.textContent = currentPost.content;
            blogTitleContainer.appendChild(description);

            contentWrapper.appendChild(blogTitleContainer);
            let blogButton = document.createElement("div");
            blogButton.setAttribute("class", "blog-button");
            let button = document.createElement("a");
            button.setAttribute("href", currentPost.link);
            button.setAttribute("target", "_blank");
            button.innerText = "Read More";
            let svgString = `
            <svg xmlns="http://www.w3.org/2000/svg" width="70" height="18" viewBox="0 0 70 18" fill="none">
              <g clip-path="url(#clip0_43_5570)">
                <path d="M1 8.93945H56.1427" stroke-width="1.99955" stroke-linecap="round" />
                <path d="M60.9844 16.264L68.3105 8.93951L60.9844 1.61499" stroke-width="1.99955"
                  stroke-linecap="round" stroke-linejoin="round" />
              </g>
              <defs>
                <clipPath id="clip0_43_5570">
                  <rect width="69.31" height="17.48" fill="white" transform="translate(0 0.199951)" />
                </clipPath>
              </defs>
            </svg>`;

            button.innerHTML += svgString;

            blogButton.appendChild(button);
            contentWrapper.appendChild(blogButton);

            blogMainContainer.appendChild(ContentContainer);

            if (postsDisplayed === 8) {
              break;
            }
            counters[currentCategory]++;
            postsDisplayed++;
          }
        })
        .catch((error) => console.error("Error fetching categories:", error));
    })
    .catch((error) => console.log("error", error));
});
