document.addEventListener("DOMContentLoaded", function() {
  var dropdownButton = document.getElementById("calendar-dropdown-button");
  var dropdownContent = document.getElementById("calendar-dropdown-content");
  let dropdownOptions = document.querySelectorAll(".calendar-option");

  // Toggle the dropdown when clicking the button
  document.body.addEventListener("click", function() {
    dropdownContent.classList.remove("open-dropdown");
  });

  dropdownButton.addEventListener("click", function(e) {
    e.stopPropagation();
    dropdownContent.classList.toggle("open-dropdown");
  });

  dropdownOptions.forEach((option) => {
    option.addEventListener("click", function() {
      dropdownContent.classList.remove("open-dropdown");
    });
  });

  // function for getting the parameters of the URL
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  // assigning the variable for the event information
  var eventTitle = getUrlParameter("eventTitle");
  var eventDate = getUrlParameter("eventDate").replace(/-/g, "");
  var eventTime = getUrlParameter("eventTime").replace(/:/g, "");
  var eventEndTime = getUrlParameter("eventEndTime").replace(/:/g, "");
  var eventEndDate = getUrlParameter("eventEndDate").replace(/-/g, "");
  var eventLink = getUrlParameter("eventLink");

  // functions for date and time formatting
  function formatGoogleCalendarDate(date, time, endTime) {
    const formattedDate = date.replace(/-/g, "");
    const formattedTime = time.replace(/:/g, "") + "00";
    const formattedEndTime = endTime.replace(/:/g, "") + "00";
    return (
      formattedDate +
      "T" +
      formattedTime +
      "/" +
      formattedDate +
      "T" +
      formattedEndTime
    );
  }
  // initial call of formatting function
  var formattedEventDate = formatGoogleCalendarDate(
    eventDate,
    eventTime,
    eventEndTime
  );

  // event call for the add to google calendar
  document
    .getElementById("add-to-google-calendar")
    .addEventListener("click", function() {
      var googleCalendarUrl =
        "https://www.google.com/calendar/render?action=TEMPLATE" +
        "&text=" +
        eventTitle +
        "&dates=" +
        formattedEventDate +
        "&details=" +
        "\n To join the live event, just click on this link.: " +
        eventLink +
        "&location=" +
        eventLink;
      window.open(googleCalendarUrl, "_blank");
    });


    // event call function for add to outlook calendar
  document
    .getElementById("add-to-outlook-calendar")
    .addEventListener("click", function() {
      var calendarContent =
        "BEGIN:VCALENDAR\n" +
        "VERSION:2.0\n" +
        "BEGIN:VEVENT\n" +
        "SUMMARY:" +
        eventTitle +
        "\n" +
        "DTSTART:" +
        eventDate +
        "T" +
        eventTime.replace(/:/g, "") +
        "00\n" +
        "DTEND:" +
        eventEndDate +
        "T" +
        eventEndTime.replace(/:/g, "") +
        "00\n" +
        "LOCATION:" +
        eventLink +
        "\n" +
        "DESCRIPTION:" +
        "\n To join the live event, just click on this link.:" +
        eventLink +
        "\n" +
        "END:VEVENT\n" +
        "END:VCALENDAR";
      var blob = new Blob([calendarContent], {
        type: "text/calendar;charset=utf-8",
      });
      var blobUrl = URL.createObjectURL(blob);
      var link = document.createElement("a");
      link.href = blobUrl;
      link.download = eventTitle;
      link.click();
    });

  // event call function for add to apple calendar
  document
    .getElementById("add-to-apple-calendar")
    .addEventListener("click", function() {
      var calendarContent =
        "BEGIN:VCALENDAR\n" +
        "VERSION:2.0\n" +
        "BEGIN:VEVENT\n" +
        "SUMMARY:" +
        eventTitle +
        "\n" +
        "DTSTART:" +
        eventDate +
        "T" +
        eventTime +
        "\n" +
        "DTEND:" +
        eventEndDate +
        "T" +
        eventEndTime +
        "\n" +
        "LOCATION:" +
        eventLink +
        "\n" +
        "DESCRIPTION:" +
        "\n To join the live event, just click on this link.:" +
        eventLink +
        "\n" +
        "END:VEVENT\n" +
        "END:VCALENDAR";

      var blob = new Blob([calendarContent], {
        type: "text/calendar;charset=utf-8",
      });
      var blobUrl = URL.createObjectURL(blob);

      var link = document.createElement("a");
      link.href = blobUrl;
      link.download = eventTitle;

      link.click();
    });
});
