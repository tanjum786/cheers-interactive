$(document).ready(function() {
  $(".agenda-event-tabs h6:first").addClass("active-tab");
  $(".agenda-tab-content:first").addClass("custom-class"); 


  $(".agenda-event-tabs h6").click(function() {
    $(".agenda-event-tabs h6").removeClass("active-tab");
    $(".agenda-tab-content").removeClass("custom-class");


    var index = $(this).index();
    $(".agenda-tab-content")
      .eq(index)
      .addClass("custom-class");
    $(this).addClass("active-tab");
  });

  $(".drop-down").click(function() {
    $(this)
      .closest(".agenda-event-wrapper")
      .find(".agenda-discription")
      .toggleClass("show");
    $(this).toggleClass("rotate-down");
  });
});
