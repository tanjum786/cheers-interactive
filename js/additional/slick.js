!function(factory) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], factory) : "undefined" != typeof exports ? module.exports = factory(require("jquery")) : factory(jQuery)
}((function($) {
    "use strict";
    var instanceUid, Slick = window.Slick || {};
    instanceUid = 0,
    (Slick = function(element, settings) {  }
    ).prototype.activateADA = function() {
        this.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        })
    }
    ,
    Slick.prototype.addSlide = Slick.prototype.slickAdd = function(markup, index, addBefore) {  }
    ,
    Slick.prototype.animateHeight = function() {
        var _ = this;
        if (1 === _.options.slidesToShow && !0 === _.options.adaptiveHeight && !1 === _.options.vertical) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(!0);
            _.$list.animate({
                height: targetHeight
            }, _.options.speed)
        }
    }
    ,
    Slick.prototype.animateSlide = function(targetLeft, callback) {  }
    ,
    Slick.prototype.getNavTarget = function() {
        var asNavFor = this.options.asNavFor;
        return asNavFor && null !== asNavFor && (asNavFor = $(asNavFor).not(this.$slider)),
        asNavFor
    }
    ,
    Slick.prototype.asNavFor = function(index) {
        var asNavFor = this.getNavTarget();
        null !== asNavFor && "object" == typeof asNavFor && asNavFor.each((function() {
            var target = $(this).slick("getSlick");
            target.unslicked || target.slideHandler(index, !0)
        }
        ))
    }
    ,
    Slick.prototype.applyTransition = function(slide) {
        var _ = this
          , transition = {};
        !1 === _.options.fade ? transition[_.transitionType] = _.transformType + " " + _.options.speed + "ms " + _.options.cssEase : transition[_.transitionType] = "opacity " + _.options.speed + "ms " + _.options.cssEase,
        !1 === _.options.fade ? _.$slideTrack.css(transition) : _.$slides.eq(slide).css(transition)
    }
    ,
    Slick.prototype.autoPlay = function() {
        var _ = this;
        _.autoPlayClear(),
        _.slideCount > _.options.slidesToShow && (_.autoPlayTimer = setInterval(_.autoPlayIterator, _.options.autoplaySpeed))
    }
    ,
    Slick.prototype.autoPlayClear = function() {
        this.autoPlayTimer && clearInterval(this.autoPlayTimer)
    }
    ,
    Slick.prototype.autoPlayIterator = function() {
        var _ = this
          , slideTo = _.currentSlide + _.options.slidesToScroll;
        _.paused || _.interrupted || _.focussed || (!1 === _.options.infinite && (1 === _.direction && _.currentSlide + 1 === _.slideCount - 1 ? _.direction = 0 : 0 === _.direction && (slideTo = _.currentSlide - _.options.slidesToScroll,
        _.currentSlide - 1 == 0 && (_.direction = 1))),
        _.slideHandler(slideTo))
    }
    ,
    Slick.prototype.buildArrows = function() {
        var _ = this;
        !0 === _.options.arrows && (_.$prevArrow = $(_.options.prevArrow).addClass("slick-arrow"),
        _.$nextArrow = $(_.options.nextArrow).addClass("slick-arrow"),
        _.slideCount > _.options.slidesToShow ? (_.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
        _.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
        _.htmlExpr.test(_.options.prevArrow) && _.$prevArrow.prependTo(_.options.appendArrows),
        _.htmlExpr.test(_.options.nextArrow) && _.$nextArrow.appendTo(_.options.appendArrows),
        !0 !== _.options.infinite && _.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : _.$prevArrow.add(_.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    }
    ,
    Slick.prototype.buildDots = function() {
        var i, dot, _ = this;
        if (!0 === _.options.dots && _.slideCount > _.options.slidesToShow) {
            for (_.$slider.addClass("slick-dotted"),
            dot = $("<ul />").addClass(_.options.dotsClass),
            i = 0; i <= _.getDotCount(); i += 1)
                dot.append($("<li />").append(_.options.customPaging.call(this, _, i)));
            _.$dots = dot.appendTo(_.options.appendDots),
            _.$dots.find("li").first().addClass("slick-active")
        }
    }
    ,
    Slick.prototype.buildOut = function() {  }
    ,
    Slick.prototype.buildRows = function() {  }
    ,
    Slick.prototype.checkResponsive = function(initial, forceUpdate) { }
    ,
    Slick.prototype.changeSlide = function(event, dontAnimate) {  }
    ,
    Slick.prototype.checkNavigable = function(index) {
        var navigables, prevNavigable;
        if (prevNavigable = 0,
        index > (navigables = this.getNavigableIndexes())[navigables.length - 1])
            index = navigables[navigables.length - 1];
        else
            for (var n in navigables) {
                if (index < navigables[n]) {
                    index = prevNavigable;
                    break
                }
                prevNavigable = navigables[n]
            }
        return index
    }
    ,
    Slick.prototype.cleanUpEvents = function() {  }
    ,
    Slick.prototype.cleanUpSlideEvents = function() {
        var _ = this;
        _.$list.off("mouseenter.slick", $.proxy(_.interrupt, _, !0)),
        _.$list.off("mouseleave.slick", $.proxy(_.interrupt, _, !1))
    }
    ,
    Slick.prototype.cleanUpRows = function() {
        var originalSlides, _ = this;
        _.options.rows > 0 && ((originalSlides = _.$slides.children().children()).removeAttr("style"),
        _.$slider.empty().append(originalSlides))
    }
    ,
    Slick.prototype.clickHandler = function(event) {
        !1 === this.shouldClick && (event.stopImmediatePropagation(),
        event.stopPropagation(),
        event.preventDefault())
    }
    ,
    Slick.prototype.destroy = function(refresh) {  }
    ,
    Slick.prototype.disableTransition = function(slide) {
        var _ = this
          , transition = {};
        transition[_.transitionType] = "",
        !1 === _.options.fade ? _.$slideTrack.css(transition) : _.$slides.eq(slide).css(transition)
    }
    ,
    Slick.prototype.fadeSlide = function(slideIndex, callback) { }
    ,
    Slick.prototype.fadeSlideOut = function(slideIndex) {
        var _ = this;
        !1 === _.cssTransitions ? _.$slides.eq(slideIndex).animate({
            opacity: 0,
            zIndex: _.options.zIndex - 2
        }, _.options.speed, _.options.easing) : (_.applyTransition(slideIndex),
        _.$slides.eq(slideIndex).css({
            opacity: 0,
            zIndex: _.options.zIndex - 2
        }))
    }
    ,
    Slick.prototype.filterSlides = Slick.prototype.slickFilter = function(filter) {
        var _ = this;
        null !== filter && (_.$slidesCache = _.$slides,
        _.unload(),
        _.$slideTrack.children(this.options.slide).detach(),
        _.$slidesCache.filter(filter).appendTo(_.$slideTrack),
        _.reinit())
    }
    ,
    Slick.prototype.focusHandler = function() {
        var _ = this;
        _.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", (function(event) {
            event.stopImmediatePropagation();
            var $sf = $(this);
            setTimeout((function() {
                _.options.pauseOnFocus && (_.focussed = $sf.is(":focus"),
                _.autoPlay())
            }
            ), 0)
        }
        ))
    }
    ,
    Slick.prototype.getCurrent = Slick.prototype.slickCurrentSlide = function() {
        return this.currentSlide
    }
    ,
    Slick.prototype.getDotCount = function() { }
    ,
    Slick.prototype.getLeft = function(slideIndex) { }
    ,
    Slick.prototype.getOption = Slick.prototype.slickGetOption = function(option) {
        return this.options[option]
    }
    ,
    Slick.prototype.getNavigableIndexes = function() {
        var max, _ = this, breakPoint = 0, counter = 0, indexes = [];
        for (!1 === _.options.infinite ? max = _.slideCount : (breakPoint = -1 * _.options.slidesToScroll,
        counter = -1 * _.options.slidesToScroll,
        max = 2 * _.slideCount); breakPoint < max; )
            indexes.push(breakPoint),
            breakPoint = counter + _.options.slidesToScroll,
            counter += _.options.slidesToScroll <= _.options.slidesToShow ? _.options.slidesToScroll : _.options.slidesToShow;
        return indexes
    }
    ,
    Slick.prototype.getSlick = function() {
        return this
    }
    ,
    Slick.prototype.getSlideCount = function() {
        var swipedSlide, centerOffset, _ = this;
        return centerOffset = !0 === _.options.centerMode ? _.slideWidth * Math.floor(_.options.slidesToShow / 2) : 0,
        !0 === _.options.swipeToSlide ? (_.$slideTrack.find(".slick-slide").each((function(index, slide) {
            if (slide.offsetLeft - centerOffset + $(slide).outerWidth() / 2 > -1 * _.swipeLeft)
                return swipedSlide = slide,
                !1
        }
        )),
        Math.abs($(swipedSlide).attr("data-slick-index") - _.currentSlide) || 1) : _.options.slidesToScroll
    }
    ,
    Slick.prototype.goTo = Slick.prototype.slickGoTo = function(slide, dontAnimate) {
        this.changeSlide({
            data: {
                message: "index",
                index: parseInt(slide)
            }
        }, dontAnimate)
    }
    ,
    Slick.prototype.init = function(creation) {  }
    ,
    Slick.prototype.initADA = function() { }
    ,
    Slick.prototype.initArrowEvents = function() {
        var _ = this;
        !0 === _.options.arrows && _.slideCount > _.options.slidesToShow && (_.$prevArrow.off("click.slick").on("click.slick", {
            message: "previous"
        }, _.changeSlide),
        _.$nextArrow.off("click.slick").on("click.slick", {
            message: "next"
        }, _.changeSlide),
        !0 === _.options.accessibility && (_.$prevArrow.on("keydown.slick", _.keyHandler),
        _.$nextArrow.on("keydown.slick", _.keyHandler)))
    }
    ,
    Slick.prototype.initDotEvents = function() {
        var _ = this;
        !0 === _.options.dots && _.slideCount > _.options.slidesToShow && ($("li", _.$dots).on("click.slick", {
            message: "index"
        }, _.changeSlide),
        !0 === _.options.accessibility && _.$dots.on("keydown.slick", _.keyHandler)),
        !0 === _.options.dots && !0 === _.options.pauseOnDotsHover && _.slideCount > _.options.slidesToShow && $("li", _.$dots).on("mouseenter.slick", $.proxy(_.interrupt, _, !0)).on("mouseleave.slick", $.proxy(_.interrupt, _, !1))
    }
    ,
    Slick.prototype.initSlideEvents = function() {
        var _ = this;
        _.options.pauseOnHover && (_.$list.on("mouseenter.slick", $.proxy(_.interrupt, _, !0)),
        _.$list.on("mouseleave.slick", $.proxy(_.interrupt, _, !1)))
    }
    ,
    Slick.prototype.initializeEvents = function() {  }
    ,
    Slick.prototype.initUI = function() {
        var _ = this;
        !0 === _.options.arrows && _.slideCount > _.options.slidesToShow && (_.$prevArrow.show(),
        _.$nextArrow.show()),
        !0 === _.options.dots && _.slideCount > _.options.slidesToShow && _.$dots.show()
    }
    ,
    Slick.prototype.keyHandler = function(event) {
        var _ = this;
        event.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === event.keyCode && !0 === _.options.accessibility ? _.changeSlide({
            data: {
                message: !0 === _.options.rtl ? "next" : "previous"
            }
        }) : 39 === event.keyCode && !0 === _.options.accessibility && _.changeSlide({
            data: {
                message: !0 === _.options.rtl ? "previous" : "next"
            }
        }))
    }
    ,
    Slick.prototype.lazyLoad = function() {  }
    ,
    Slick.prototype.loadSlider = function() {
        var _ = this;
        _.setPosition(),
        _.$slideTrack.css({
            opacity: 1
        }),
        _.$slider.removeClass("slick-loading"),
        _.initUI(),
        "progressive" === _.options.lazyLoad && _.progressiveLazyLoad()
    }
    ,
    Slick.prototype.next = Slick.prototype.slickNext = function() {
        this.changeSlide({
            data: {
                message: "next"
            }
        })
    }
    ,
    Slick.prototype.orientationChange = function() {
        this.checkResponsive(),
        this.setPosition()
    }
    ,
    Slick.prototype.pause = Slick.prototype.slickPause = function() {
        this.autoPlayClear(),
        this.paused = !0
    }
    ,
    Slick.prototype.play = Slick.prototype.slickPlay = function() {
        var _ = this;
        _.autoPlay(),
        _.options.autoplay = !0,
        _.paused = !1,
        _.focussed = !1,
        _.interrupted = !1
    }
    ,
    Slick.prototype.postSlide = function(index) {
        var _ = this;
        _.unslicked || (_.$slider.trigger("afterChange", [_, index]),
        _.animating = !1,
        _.slideCount > _.options.slidesToShow && _.setPosition(),
        _.swipeLeft = null,
        _.options.autoplay && _.autoPlay(),
        !0 === _.options.accessibility && (_.initADA(),
        _.options.focusOnChange && $(_.$slides.get(_.currentSlide)).attr("tabindex", 0).focus()))
    }
    ,
    Slick.prototype.prev = Slick.prototype.slickPrev = function() {
        this.changeSlide({
            data: {
                message: "previous"
            }
        })
    }
    ,
    Slick.prototype.preventDefault = function(event) {
        event.preventDefault()
    }
    ,
    Slick.prototype.progressiveLazyLoad = function(tryCount) {  }
    ,
    Slick.prototype.refresh = function(initializing) {  }
    ,
    Slick.prototype.registerBreakpoints = function() { }
    ,
    Slick.prototype.reinit = function() {  }
    ,
    Slick.prototype.resize = function() {
        var _ = this;
        $(window).width() !== _.windowWidth && (clearTimeout(_.windowDelay),
        _.windowDelay = window.setTimeout((function() {
            _.windowWidth = $(window).width(),
            _.checkResponsive(),
            _.unslicked || _.setPosition()
        }
        ), 50))
    }
    ,
    Slick.prototype.removeSlide = Slick.prototype.slickRemove = function(index, removeBefore, removeAll) {
        var _ = this;
        if (index = "boolean" == typeof index ? !0 === (removeBefore = index) ? 0 : _.slideCount - 1 : !0 === removeBefore ? --index : index,
        _.slideCount < 1 || index < 0 || index > _.slideCount - 1)
            return !1;
        _.unload(),
        !0 === removeAll ? _.$slideTrack.children().remove() : _.$slideTrack.children(this.options.slide).eq(index).remove(),
        _.$slides = _.$slideTrack.children(this.options.slide),
        _.$slideTrack.children(this.options.slide).detach(),
        _.$slideTrack.append(_.$slides),
        _.$slidesCache = _.$slides,
        _.reinit()
    }
    ,
    Slick.prototype.setCSS = function(position) {
        var x, y, _ = this, positionProps = {};
        !0 === _.options.rtl && (position = -position),
        x = "left" == _.positionProp ? Math.ceil(position) + "px" : "0px",
        y = "top" == _.positionProp ? Math.ceil(position) + "px" : "0px",
        positionProps[_.positionProp] = position,
        !1 === _.transformsEnabled ? _.$slideTrack.css(positionProps) : (positionProps = {},
        !1 === _.cssTransitions ? (positionProps[_.animType] = "translate(" + x + ", " + y + ")",
        _.$slideTrack.css(positionProps)) : (positionProps[_.animType] = "translate3d(" + x + ", " + y + ", 0px)",
        _.$slideTrack.css(positionProps)))
    }
    ,
    Slick.prototype.setDimensions = function() { }
    ,
    Slick.prototype.setFade = function() {  }
    ,
    Slick.prototype.setHeight = function() {
        var _ = this;
        if (1 === _.options.slidesToShow && !0 === _.options.adaptiveHeight && !1 === _.options.vertical) {
            var targetHeight = _.$slides.eq(_.currentSlide).outerHeight(!0);
            _.$list.css("height", targetHeight)
        }
    }
    ,
    Slick.prototype.setOption = Slick.prototype.slickSetOption = function() {   }
    ,
    Slick.prototype.setPosition = function() {
        var _ = this;
        _.setDimensions(),
        _.setHeight(),
        !1 === _.options.fade ? _.setCSS(_.getLeft(_.currentSlide)) : _.setFade(),
        _.$slider.trigger("setPosition", [_])
    }
    ,
    Slick.prototype.setProps = function() {  }
    ,
    Slick.prototype.setSlideClasses = function(index) {   }
    ,
    Slick.prototype.setupInfinite = function() {    }
    ,
    Slick.prototype.interrupt = function(toggle) {
        toggle || this.autoPlay(),
        this.interrupted = toggle
    }
    ,
    Slick.prototype.selectHandler = function(event) {
        var _ = this
          , targetElement = $(event.target).is(".slick-slide") ? $(event.target) : $(event.target).parents(".slick-slide")
          , index = parseInt(targetElement.attr("data-slick-index"));
        index || (index = 0),
        _.slideCount <= _.options.slidesToShow ? _.slideHandler(index, !1, !0) : _.slideHandler(index)
    }
    ,
    Slick.prototype.slideHandler = function(index, sync, dontAnimate) {  }
    ,
    Slick.prototype.startLoad = function() {
        var _ = this;
        !0 === _.options.arrows && _.slideCount > _.options.slidesToShow && (_.$prevArrow.hide(),
        _.$nextArrow.hide()),
        !0 === _.options.dots && _.slideCount > _.options.slidesToShow && _.$dots.hide(),
        _.$slider.addClass("slick-loading")
    }
    ,
    Slick.prototype.swipeDirection = function() {
        var xDist, yDist, r, swipeAngle, _ = this;
        return xDist = _.touchObject.startX - _.touchObject.curX,
        yDist = _.touchObject.startY - _.touchObject.curY,
        r = Math.atan2(yDist, xDist),
        (swipeAngle = Math.round(180 * r / Math.PI)) < 0 && (swipeAngle = 360 - Math.abs(swipeAngle)),
        swipeAngle <= 45 && swipeAngle >= 0 || swipeAngle <= 360 && swipeAngle >= 315 ? !1 === _.options.rtl ? "left" : "right" : swipeAngle >= 135 && swipeAngle <= 225 ? !1 === _.options.rtl ? "right" : "left" : !0 === _.options.verticalSwiping ? swipeAngle >= 35 && swipeAngle <= 135 ? "down" : "up" : "vertical"
    }
    ,
    Slick.prototype.swipeEnd = function(event) {   }
    ,
    Slick.prototype.swipeHandler = function(event) {  }
    ,
    Slick.prototype.swipeMove = function(event) {   }
    ,
    Slick.prototype.swipeStart = function(event) {
        var touches, _ = this;
        if (_.interrupted = !0,
        1 !== _.touchObject.fingerCount || _.slideCount <= _.options.slidesToShow)
            return _.touchObject = {},
            !1;
        void 0 !== event.originalEvent && void 0 !== event.originalEvent.touches && (touches = event.originalEvent.touches[0]),
        _.touchObject.startX = _.touchObject.curX = void 0 !== touches ? touches.pageX : event.clientX,
        _.touchObject.startY = _.touchObject.curY = void 0 !== touches ? touches.pageY : event.clientY,
        _.dragging = !0
    }
    ,
    Slick.prototype.unfilterSlides = Slick.prototype.slickUnfilter = function() {  }
    ,
    Slick.prototype.unload = function() {
        var _ = this;
        $(".slick-cloned", _.$slider).remove(),
        _.$dots && _.$dots.remove(),
        _.$prevArrow && _.htmlExpr.test(_.options.prevArrow) && _.$prevArrow.remove(),
        _.$nextArrow && _.htmlExpr.test(_.options.nextArrow) && _.$nextArrow.remove(),
        _.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }
    ,
    Slick.prototype.unslick = function(fromBreakpoint) {
        var _ = this;
        _.$slider.trigger("unslick", [_, fromBreakpoint]),
        _.destroy()
    }
    ,
    Slick.prototype.updateArrows = function() {  }
    ,
    Slick.prototype.updateDots = function() {
        var _ = this;
        null !== _.$dots && (_.$dots.find("li").removeClass("slick-active").end(),
        _.$dots.find("li").eq(Math.floor(_.currentSlide / _.options.slidesToScroll)).addClass("slick-active"))
    }
    ,
    Slick.prototype.visibility = function() {
        var _ = this;
        _.options.autoplay && (document[_.hidden] ? _.interrupted = !0 : _.interrupted = !1)
    }
    ,
    $.fn.slick = function() { }
}
));
//# sourceURL=https://cdn2.hubspot.net/hub/4016590/hub_generated/template_assets/91548028321/1669285814905/Hubspot-Marketplace-Assets/2022/Product-Launch/js/additional/slick.js
