(function() {
  // Variables
  var nav = document.querySelector(".header__navigation");
  var langSwitcher = document.querySelector(".header__language-switcher");
  var search = document.querySelector(".header__search");
  var allToggles = document.querySelectorAll(".header--toggle");
  var navToggle = document.querySelector(".header__navigation--toggle");
  var langToggle = document.querySelector(".header__language-switcher--toggle");
  var searchToggle = document.querySelector(".header__search--toggle");
  var closeToggle = document.querySelector(".header__close--toggle");
  var allElements = document.querySelectorAll(
    ".header--element, .header--toggle"
  );
  var emailGlobalUnsub = document.querySelector('input[name="globalunsub"]');

  // Functions

  // Function for executing code on document ready
  function domReady(callback) {
    if (["interactive", "complete"].indexOf(document.readyState) >= 0) {
      callback();
    } else {
      document.addEventListener("DOMContentLoaded", callback);
    }
  }

  // Function for toggling mobile navigation
  function toggleNav() {
    allToggles.forEach(function(toggle) {
      toggle.classList.toggle("hide");
    });

    nav.classList.toggle("open");
    navToggle.classList.toggle("open");

    closeToggle.classList.toggle("show");
  }

  // Function for toggling mobile language selector
  function toggleLang() {
    allToggles.forEach(function(toggle) {
      toggle.classList.toggle("hide");
    });

    langSwitcher.classList.toggle("open");
    langToggle.classList.toggle("open");

    closeToggle.classList.toggle("show");
  }

  // Function for toggling mobile search field
  function toggleSearch() {
    allToggles.forEach(function(toggle) {
      toggle.classList.toggle("hide");
    });

    search.classList.toggle("open");
    searchToggle.classList.toggle("open");

    closeToggle.classList.toggle("show");
  }

  // Function for the header close option on mobile
  function closeAll() {
    allElements.forEach(function(element) {
      element.classList.remove("hide", "open");
    });

    closeToggle.classList.remove("show");
  }

  // Function to disable the other checkbox inputs on the email subscription system page template
  function toggleDisabled() {
    var emailSubItem = document.querySelectorAll("#email-prefs-form .item");

    emailSubItem.forEach(function(item) {
      var emailSubItemInput = item.querySelector("input");

      if (emailGlobalUnsub.checked) {
        item.classList.add("disabled");
        emailSubItemInput.setAttribute("disabled", "disabled");
        emailSubItemInput.checked = false;
      } else {
        item.classList.remove("disabled");
        emailSubItemInput.removeAttribute("disabled");
      }
    });
  }

  // Execute JavaScript on document ready
  domReady(function() {
    if (!document.body) {
      return;
    } else {
      // Function dependent on language switcher
      if (langSwitcher) {
        langToggle.addEventListener("click", toggleLang);
      }

      // Function dependent on navigation
      if (navToggle) {
        navToggle.addEventListener("click", toggleNav);
      }

      // Function dependent on search field
      if (searchToggle) {
        searchToggle.addEventListener("click", toggleSearch);
      }

      // Function dependent on close toggle
      if (closeToggle) {
        closeToggle.addEventListener("click", closeAll);
      }

      // Function dependent on email unsubscribe from all input
      if (emailGlobalUnsub) {
        emailGlobalUnsub.addEventListener("change", toggleDisabled);
      }
    }
  });
})();

// future Insight slider
$(document).ready(function() {
  // logo
  $(".logo-container").slick({
    dots: false,
    infinite: true,
    speed: 250,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 999,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  // testimonial

  $(".testimonial-slider").slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    // adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
});

// tab content
document.addEventListener("DOMContentLoaded", function() {
  let tabsContainers = document.querySelectorAll(".tabs-wrapper");

  tabsContainers.forEach((container) => {
    let tabs = container.querySelectorAll(".tabs");
    let tabContents = container.querySelectorAll(".tabs_content");

    tabs.forEach((tab, index) => {
      tab.addEventListener("click", () => {
        tabContents.forEach((content) => {
          content.classList.remove("actives");
        });
        tabs.forEach((tab) => {
          tab.classList.remove("actives");
        });
        tabContents[index].classList.add("actives");
        tabs[index].classList.add("actives");
      });
    });
  });
});

window.onload = function() {
  var tabs = document.querySelectorAll(".tabs");
  tabs.forEach(function(tab) {
    var tabsContentActives = document.querySelectorAll(".tabs_content.actives");
    tabsContentActives.forEach(function(tabsContentActive) {
      var imageContainer = tabsContentActive.querySelector(
        ".tabs_content-image"
      );
      var image = imageContainer.querySelector("img");
      if (!image) {
        var text = tabsContentActive.querySelector(".tabs_content-text");
        text.style.width = "100%";
        imageContainer.style.width = "0";
      }
    });
  });
};

//  section Active Scroll
document.addEventListener("DOMContentLoaded", function() {
  const menuItems = document.querySelectorAll(".tabbing-wrapper a");
  const sectionElements = document.querySelectorAll(".section-id");

  const options = {
    root: null,
    rootMargin: "0px",
    threshold: 0.3,
  };

  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      const targetId = entry.target.getAttribute("id");
      const menuItemToActivate = document.querySelector(
        `.tabbing-wrapper a[href="#${targetId}"]`
      );

      if (entry.isIntersecting) {
        menuItemToActivate?.classList.add("active");
      } else {
        menuItemToActivate?.classList.remove("active");
      }
    });
  }, options);

  sectionElements.forEach((section) => {
    observer.observe(section);
  });

  menuItems.forEach((item) => {
    item.addEventListener("click", function(event) {
      event.preventDefault();
      const targetId = this.getAttribute("href").substring(1);
      scrollToElement(targetId);
      window.history.replaceState(null, null, `#${targetId}`);
    });
  });

  function scrollToElement(targetId) {
    const targetSection = document.getElementById(targetId);
    if (targetSection) {
      targetSection.scrollIntoView({ behavior: "smooth" });
    }
  }
});

// function for consent auto selection
$( document ).ready(function() {
setTimeout(e => {
   $(".legal-consent-container").addClass("no-show");
   $(".legal-consent-container").append(`<div class="error-message" id="errorMessage" style="font-size:.875rem;  color:#f2545b;">You must consent to receive business relevant communication from FutureBridge.</div>`);    
   $("input[name='LEGAL_CONSENT.subscription_type_250464652']").prop("checked", true).change();


$("input[name='LEGAL_CONSENT.subscription_type_250464652']").each(function() {
  $(this).off().click(function() {
    var checkbox = $(this);

    checkbox.toggleClass("checked");

    if (!checkbox.is(":checked")) {
      checkbox.closest(".legal-consent-container").addClass("no-show show-error");
    } else {
      checkbox.closest(".legal-consent-container").removeClass("no-show show-error");
    }
  });
});
$("input[type='submit']").off().click(function() {
  
 var checkbox = $('.legal-consent-container input[type="checkbox"]');


  if (!checkbox.is(":checked")) {
    $(".legal-consent-container").addClass("no-show"); // Add the class
    $(".legal-consent-container").addClass("show-error"); 
    
    // Add the class
  } else {
    $(".legal-consent-container").removeClass("no-show"); // Remove the class
    $(".legal-consent-container").removeClass("show-error"); // Remove the class
  }
  });

}, 3000);
});
